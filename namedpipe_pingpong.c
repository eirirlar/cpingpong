#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h> 
#include <sys/stat.h> 
#include <sys/types.h> 

#define LOG_INFO(MESSAGE) fprintf(stderr, "%s\n", MESSAGE)

int main(int argc, char** argv) {
	char* reqFile = "/dev/shm/req";
	char* resFile = "/dev/shm/res";
	mkfifo(reqFile, 0666);
	mkfifo(resFile, 0666);

	int nPingPongs = 1000000;
	int forkstatus = fork();
	if (forkstatus < 0) {
		perror("fork");
		exit(1);
	}
	clock_t start_clock = clock();
	struct timeval start_tod, end_tod;
	gettimeofday(&start_tod, NULL);
	if (forkstatus == 0) {
		// Child process
		int fdRes = open(resFile, O_WRONLY);
		int fdReq = open(reqFile, O_RDONLY);
		char flag;
		int p = 0;
		while(p < nPingPongs) {
			if (read(fdReq, &flag, 1) == -1) {
				perror("child read");
				exit(-errno);
			}

			if (write(fdRes, &flag, 1) == -1) {
				perror("child write");                                                                            exit(-errno);
			}		
			p+=1;
		}
	} else {
		// Parent process.
		int fdRes = open(resFile, O_RDONLY);
		int fdReq = open(reqFile, O_WRONLY);
		char flag;	
		int p = 0;
		while(p < nPingPongs) {
			//write
			//read
			if (write(fdReq, &flag, 1) == -1) {
				perror("parent read");
				exit(-errno);
			}

			if (read(fdRes, &flag, 1) == -1) {
				perror("parent write");
				exit(-errno);
			}		
			p+=1;
		}
		// Wait for the child to terminate.
		wait(NULL);
	}
	double clock_used = ((double) (clock() - start_clock)) / CLOCKS_PER_SEC;
	gettimeofday(&end_tod, NULL);
	long tod_seconds_used = end_tod.tv_sec - start_tod.tv_sec;
	double time_used = ((double) (tod_seconds_used * 1000000 + end_tod.tv_usec - start_tod.tv_usec)) / 1000000;

	fprintf(stderr,"%i pingpongs clock: %fs, time: %fs for process %i. %f pp/s\n", nPingPongs, clock_used, time_used, forkstatus, nPingPongs/time_used);

	return 0;
}
