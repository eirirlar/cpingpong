# description
Compare speed of various process synchronization mechanisms. Thus far:
- futex
- unnamed pipe

# compile
gcc unnamedpipe_pingpong.c -o out/unnamedpipe_pingpong
gcc namedpipe_pingpong.c -o out/namedpipe_pingpong
gcc futex_pingpong.c -o out/futex_pingpong -lrt  
