#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#define LOG_INFO(MESSAGE) fprintf(stderr, "%s\n", MESSAGE)

int main(int argc, char** argv) {
	int chan_req[2]; // command channel, controller -> worker
	if (pipe(chan_req) == -1) {
		perror("pipe");
		exit(-errno);
	}
	int chan_res[2]; // response channel, worker -> controller
	if (pipe(chan_res) == -1) {
		perror("pipe");
		exit(-errno);
	}

	int nPingPongs = 1000000;
	int forkstatus = fork();
	if (forkstatus < 0) {
		perror("fork");
		exit(1);
	}
	clock_t start_clock = clock();
	struct timeval start_tod, end_tod;
	gettimeofday(&start_tod, NULL);
	if (forkstatus == 0) {
		// Child process
		close(chan_req[1]);
		close(chan_res[0]);

		char flag;
		int p = 0;
		while(p < nPingPongs) {
			if (read(chan_req[0], &flag, 1) == -1) {
				perror("child read");
				exit(-errno);
			}

			if (write(chan_res[1], &flag, 1) == -1) {
				perror("child write");                                                                            exit(-errno);
			}
			p+=1;
		}
	} else {
		// Parent process.
		close(chan_req[0]); // only writes to request channel
		close(chan_res[1]); // only reads from response channel
		char flag;
		int p = 0;
		while(p < nPingPongs) {
			if (write(chan_req[1], &flag, 1) == -1) {
				perror("master write");
				exit(-errno);
			}
			if (read(chan_res[0], &flag, 1) == -1) {
				perror("master read");
				exit(-errno);
			}
			p+=1;
		}
		// Wait for the child to terminate.
		wait(NULL);
	}
	double clock_used = ((double) (clock() - start_clock)) / CLOCKS_PER_SEC;
	gettimeofday(&end_tod, NULL);
	long tod_seconds_used = end_tod.tv_sec - start_tod.tv_sec;
	double time_used = ((double) (tod_seconds_used * 1000000 + end_tod.tv_usec - start_tod.tv_usec)) / 1000000;

	fprintf(stderr,"%i pingpongs clock: %fs, time: %fs for process %i. %f pp/s\n", nPingPongs, clock_used, time_used, forkstatus, nPingPongs/time_used);

	return 0;
}
