#include <errno.h>
#include <linux/futex.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/fcntl.h>
#include <sys/mman.h>
#include <sys/syscall.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#include <sched.h>

#define LOG_INFO(MESSAGE) fprintf(stderr, "%s\n", MESSAGE)


struct data_handle {
	void* addr;
	int err;
};


// The C runtime doesn't provide a wrapper for the futex(2) syscall, so we roll
// our own.
int futex(int* uaddr, int futex_op, int val, const struct timespec* timeout,
		int* uaddr2, int val3) {
	return syscall(SYS_futex, uaddr, futex_op, val, timeout, uaddr2, val3);
}

// Waits for the futex at futex_addr to have the value val, ignoring spurious
// wakeups. This function only returns when the condition is fulfilled; the only
// other way out is aborting with an error.
void wait_on_futex_value(int* futex_addr, int val) {
	while (1) {
		int futex_rc = futex(futex_addr, FUTEX_WAIT, val, NULL, NULL, 0);
		if (futex_rc == -1) {
			if (errno != EAGAIN) {
				perror("futex");
				exit(1);
			}
		} else if (futex_rc == 0) {
			if (*futex_addr == val) {
				// This is a real wakeup.
				return;
			}
		} else {
			abort();
		}
		sched_yield();
	}
}

// A blocking wrapper for waking a futex. Only returns when a waiter has been
// woken up.
void wake_futex_blocking(int* futex_addr) {
	while (1) {
		int futex_rc = futex(futex_addr, FUTEX_WAKE, 1, NULL, NULL, 0);
		if (futex_rc == -1) {
			perror("futex wake");
			exit(1);
		} else if (futex_rc > 0) {
			return;
		}
		sched_yield();
	}
}

static struct data_handle create_shared_memory(char const* name, size_t addr_len) {
	int fd = shm_open(name, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
	struct data_handle dh;
	if (fd == -1) {
		perror("create_dataset: shm_open");
		dh.err = -errno;
		return dh;
	}
	if (ftruncate(fd, addr_len) == -1) {
		perror("create_dataset: ftruncate");
		dh.err = -errno;
		return dh;
	}
	void* addr = mmap(NULL, addr_len, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (addr == MAP_FAILED) {
		perror("create_dataset: mmap");
		dh.err = -errno;
		return dh;
	}
	close(fd);
	dh.addr = addr;
	return dh;
}

int main(int argc, char** argv) {
	struct data_handle sm = create_shared_memory("futex_pingpong", 4);
	int* shared_data = (int*) sm.addr;
	*shared_data = 0;

	int nPingPongs = 1000000;
	int forkstatus = fork();
	if (forkstatus < 0) {
		perror("fork");
		exit(1);
	}
	clock_t start_clock = clock();
	struct timeval start_tod, end_tod;
	gettimeofday(&start_tod, NULL);
	if (forkstatus == 0) {
		int p = 0;
		// Child process
		while(p < nPingPongs) {
			wait_on_futex_value(shared_data, 0xA);
			*shared_data = 0xB;
			wake_futex_blocking(shared_data);
			p+=1;
		}
	} else {
		// Parent process.
		int p = 0;
		while(p < nPingPongs) {
			*shared_data = 0xA;
			wake_futex_blocking(shared_data);
			wait_on_futex_value(shared_data, 0xB);
			p+=1;
		}
		// Wait for the child to terminate.
		wait(NULL);
		munmap(sm.addr, 4);
		shm_unlink("futex_pingpong");
		
	}
	double clock_used = ((double) (clock() - start_clock)) / CLOCKS_PER_SEC;
	gettimeofday(&end_tod, NULL);
	long tod_seconds_used = end_tod.tv_sec - start_tod.tv_sec;
	double time_used = ((double) (tod_seconds_used * 1000000 + end_tod.tv_usec - start_tod.tv_usec)) / 1000000;

	printf("%i pingpongs clock: %fs, time: %fs for process %i. %f pp/s\n", nPingPongs, clock_used, time_used, forkstatus, nPingPongs/time_used);

	return 0;
}
